/*
 *	Main window class implemenation. 
 *
 *  Created on: Jan 30, 2015
 *  Author: Jingjin Yu
 */

#include "Boost_main_window.h"
#include "Boost_helper_functions.h"
#include <QLabel>
#include <QMenuBar>
#include <QMenu>
#include <QToolBar>
#include <QToolButton>
#include <QFileDialog>
#include <QGraphicsView>
#include <QMainWindow>
#include <QApplication>
#include <QLabel>
#include <QFile>
#include <QFileInfo>
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QMessageBox>
#include <QStatusBar>
#include <QGraphicsView>
#include <QGLWidget>
#include <QTextStream>
#include <QSettings>
#include <QUrl>
#include <QDesktopWidget>
#include <QRegExp>
//#include <QSvgGenerator>
#include <QtCore>
#include <QtOpenGL>
#if _MSC_VER == 1600
#include <qgl.h>
#endif


#include <iostream>
#include <ios>
#include <string>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp> 
#define _DEBUG

MainWindow::MainWindow( QApplication *pApp, QString& fileFolder, 
	QString title, QString iconPath, bool addDefaultContent)
:m_pApp(pApp), m_fileFolder(fileFolder), m_state(BUILD_STATE_UNKNOWN),QMainWindow()
{

	xycoord = new QLabel(" -0.00000 , -0.00000 ", this);
  	xycoord->setMinimumSize(xycoord->sizeHint());
  	xycoord->clear();

    m_pBoundingPolyAGI = 0;
	m_pBoundingPolyAGI2 = 0;

    // Set title
    setWindowTitle(title);

    // Set icon if the icon path is given
    if(iconPath.length() > 0){
        this->setWindowIcon(QIcon(iconPath));
    }


    // Check whether to display some default content
    if(addDefaultContent){
        resize(600, 400);
        QLabel * label = new QLabel(tr("Central Widget"));
        setCentralWidget(label);
        label->setAlignment(Qt::AlignCenter);
		m_pView->setMouseTracking(true);
    }
    else{
        m_pView = new QGraphicsView(this);
#if _MSC_VER == 1600
		m_pView->setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers)));
#endif

        m_scene.setItemIndexMethod(QGraphicsScene::NoIndex);
        m_scene.setSceneRect(-100, -100, 100, 100);
        m_pView->setScene(&m_scene);
        m_pView->setMouseTracking(true);

        this->view = m_pView;

        resize(620, 710);
        this->view->resize(620, 710);
        this->view->scale(1, 1);
        this->addNavigation(this->view);

        this->setupStatusBar();
        setCentralWidget(m_pView);
	}

	m_pView->setRenderHint(QPainter::Antialiasing);

	// Setup timer
	m_pTimer = new QTimer(this);
	connect(m_pTimer, SIGNAL(timeout()), this, SLOT(animatePath()));

	
	// Setup toolbar
    setupMenuAndToolbar();
}							

void MainWindow::addNavigation(QGraphicsView* graphicsView)
{
  navigation = new GraphicsViewNavigation();
  graphicsView->viewport()->installEventFilter(navigation);
  graphicsView->installEventFilter(navigation);
  QObject::connect(navigation, SIGNAL(mouseCoordinates(QString)),
		   xycoord, SLOT(setText(QString)));
  view = graphicsView;
}

void MainWindow::setupStatusBar()
{
	this->statusBar()->addWidget(new QLabel(this), 1);
	this->statusBar()->addWidget(xycoord, 0);
}

void MainWindow::setupMenuAndToolbar(){
	int iconSize = 40;
    m_pMainToolBar = new QToolBar(tr("Main Toolbar"), this);
    m_pMainToolBar->setIconSize(QSize(iconSize, iconSize));
    addToolBar(m_pMainToolBar);
    // m_pMainToolBar->setVisible(false);


    // Create the environment menu
    QMenu* fileMenu = menuBar()->addMenu(tr("&Environment"));

    // Open action
    m_pOpenEnvAction = new QAction(tr("&Open Environment"), this);
    m_pOpenEnvAction->setStatusTip(tr("Open a new envrionment"));
    m_pOpenEnvAction->setIcon(QIcon("./resources/open.png"));
    connect(m_pOpenEnvAction, SIGNAL(triggered()), this, SLOT(openEnvironment()));
    fileMenu->addAction(m_pOpenEnvAction);
    m_pMainToolBar->addAction(m_pOpenEnvAction);

	m_pMainToolBar->addSeparator();

    // Connect graph action
    m_pOverlayLatticAction = new QAction(tr("&Overlay Lattice Graph"), this);
    m_pOverlayLatticAction->setStatusTip(tr("Overlay covering lattice graph"));
    m_pOverlayLatticAction->setIcon(QIcon("./resources/hexgrid.png"));
    connect(m_pOverlayLatticAction, SIGNAL(triggered()), this, SLOT(overlayLattice()));
	fileMenu->addSeparator();
    fileMenu->addAction(m_pOverlayLatticAction);
    m_pMainToolBar->addAction(m_pOverlayLatticAction);
	m_pOverlayLatticAction->setDisabled(true); // Disabled at beginning 

    m_pLocateBoundaryAction = new QAction(tr("&Trim Lattice Graph"), this);
    m_pLocateBoundaryAction->setStatusTip(tr("Remove lattice parts outside the free space"));
    m_pLocateBoundaryAction->setIcon(QIcon("./resources/hexcycle.png"));
    connect(m_pLocateBoundaryAction, SIGNAL(triggered()), this, SLOT(locateBoundingLatticeCycle()));
    fileMenu->addAction(m_pLocateBoundaryAction);
    m_pMainToolBar->addAction(m_pLocateBoundaryAction);
	m_pLocateBoundaryAction->setDisabled(true); // Disabled at beginning 

	QWidget* empty3 = new QWidget();
	empty3->setFixedWidth(4);
	m_pMainToolBar->addWidget(empty3);

	m_pNumberofRobotLabel = new QLabel(this);
	m_pNumberofRobotLabel->setText(" Robots: ");
	m_pNumberofRobotLabel->setFixedHeight(iconSize-2);
	m_pMainToolBar->addWidget(m_pNumberofRobotLabel);

	m_pLineEdit = new QLineEdit(this);
	m_pLineEdit->setFixedWidth(30);
	m_pLineEdit->setFixedHeight(iconSize-2);
	m_pLineEdit->setText(QString("25"));
	m_pMainToolBar->addWidget(m_pLineEdit);

	QWidget* empty1 = new QWidget();
	empty1->setFixedWidth(4);
	m_pMainToolBar->addWidget(empty1);

	m_pMinDistLabel = new QLabel(this);
	m_pMinDistLabel->setText(" Spacing: ");
	m_pMinDistLabel->setFixedHeight(iconSize-2);
	m_pMainToolBar->addWidget(m_pMinDistLabel);

	m_pMinDistLineEdit = new QLineEdit(this);
	m_pMinDistLineEdit->setFixedWidth(30);
	m_pMinDistLineEdit->setFixedHeight(iconSize-2);
	m_pMinDistLineEdit->setText(QString("3"));
	m_pMainToolBar->addWidget(m_pMinDistLineEdit);

	QWidget* empty2 = new QWidget();
	empty2->setFixedWidth(4);
	m_pMainToolBar->addWidget(empty2);

	m_pCreateAction = new QAction(tr("&Create Random Problem"), this);
    m_pCreateAction->setStatusTip(tr("Create a random instance"));
    m_pCreateAction->setIcon(QIcon("./resources/sg.png"));
    connect(m_pCreateAction, SIGNAL(triggered()), this, SLOT(createRandomProblem()));
    fileMenu->addAction(m_pCreateAction);
    m_pMainToolBar->addAction(m_pCreateAction);
	m_pCreateAction->setDisabled(true); // Disabled at beginning 

    m_pSolveAction = new QAction(tr("&Solve"), this);
    m_pSolveAction->setStatusTip(tr("Solve a randomly created instance"));
    m_pSolveAction->setIcon(QIcon("./resources/solution.png"));
    connect(m_pSolveAction, SIGNAL(triggered()), this, SLOT(solveProblem()));
    fileMenu->addAction(m_pSolveAction);
    m_pMainToolBar->addAction(m_pSolveAction);
	m_pSolveAction->setDisabled(true); // Disabled at beginning 

    m_pPlayAction = new QAction(tr("&Animate"), this);
    m_pPlayAction->setStatusTip(tr("Animate the solution"));
    m_pPlayAction->setIcon(QIcon("./resources/play.png"));
    connect(m_pPlayAction, SIGNAL(triggered()), this, SLOT(animate()));
    fileMenu->addAction(m_pPlayAction);
    m_pMainToolBar->addAction(m_pPlayAction);
	m_pPlayAction->setDisabled(true); // Disabled at beginning 

    // Fit to screen action
	m_pFitScreenAction = new QAction(tr("&Fit content to view"), this);
    m_pFitScreenAction->setStatusTip(tr("Fit content to view"));
    m_pFitScreenAction->setIcon(QIcon("./resources/fitview.png"));
    connect(m_pFitScreenAction, SIGNAL(triggered()), this, SLOT(fitView()));
	fileMenu->addSeparator();
    fileMenu->addAction(m_pFitScreenAction);
    m_pMainToolBar->addAction(m_pFitScreenAction);

    // m_pTestAction = new QAction(tr("&Test"), this);
    // m_pTestAction->setStatusTip(tr("Run some test"));
    // m_pTestAction->setIcon(QIcon("./resources/test.jpg"));
    // connect(m_pTestAction, SIGNAL(triggered()), this, SLOT(singleTest()));
    // fileMenu->addAction(m_pTestAction);
    // m_pMainToolBar->addAction(m_pTestAction);
    // m_pTestAction->setDisabled(false); // Disabled at beginning 
}

void MainWindow::openEnvironment(){
	// Open file dialog for file selection
    QString m_fileNameq = QFileDialog::getOpenFileName(this, tr("Open Environment Description File"), "", tr("Files (*.*)"));
    if(m_fileNameq == 0 || m_fileNameq.length() < 0) return;

	QStringList qsl = m_fileNameq.split("/");
	m_envFileName = qsl[qsl.size()-1].split(".")[0];
	std::cout<<qPrintable(m_fileNameq);
    m_fileName = m_fileNameq.toStdString();
    std::cout<<m_fileName;
    m_numRobots = m_pLineEdit->text().toInt();
	// Clean up
    m_scene.clear();
    m_boundingPoly.clear();
	m_boundingPoly2.clear();
	m_envPolyList.clear();
	m_envObsPolyList.clear();
	m_PolyAGIList.clear(); /* The element pointers are gone when the scene object is cleared */

	// Reading in the environment, first the raidus of the (disc) robot
    std::ifstream ifs(qPrintable(m_fileNameq));
    double radius;
    ifs >> radius;

	// Then the number of obstacle polygons
    int numberOfPolygons;
    ifs >> numberOfPolygons;
	
	// Then read in all obstacle polygons and compute the configuration space for a single disc
    for(int i = 0; i < numberOfPolygons; i ++){
        // Load polygon
        Polygon_2 tp;
        int numberOfVertex;
        ifs >> numberOfVertex;
		//Point_2 firstOne_1;
        for(int j = 0;j < numberOfVertex;j++){
        	Point_2 p;
        	double p_x, p_y;
        	ifs >> p_x >> p_y;
			// if(j == 0){
   //      		firstOne_1.set<0>(p_x);
   //      		firstOne_1.set<1>(p_y);
   //      	}
        	p.set<0>(p_x);
        	p.set<1>(p_y);
        	bg::append(tp.outer(), p);
        }
       // bg::append(tp.outer(), firstOne_1);
		bg::correct(tp);
        m_envPolyList.push_back(tp);


		// Add raw obstacle to scene and set fill to be true with fill transparency
        AdvancedGraphicsItem<Polygon_2>* pAGI = new AdvancedGraphicsItem<Polygon_2>(&(m_envPolyList.back()));
        
        // m_scene.addItem(pAGI);
        pAGI->m_bShowEdge = true;
        pAGI->m_bShowVertices = true;
        pAGI->m_bFill = false;
        pAGI->m_fillBrush = QColor(16,16,16,192);
		m_PolyAGIList.push_back(pAGI);

        // Computing the Minkowski sum
        int split_num = 8;
        Polygon_2 ep = growPolygonByRadius(tp, radius, split_num);
		bg::correct(ep);
        //split_num = 4;
        //Polygon_2 vp = growPolygonByRadius(tp, radius, split_num);
        //bg::correct(vp);

        m_envPolyVoronoiList.push_back(ep);
		m_envObsPolyList.push_back(ep);

		// Add the configuration space obstacle to the scene
        pAGI = new AdvancedGraphicsItem<Polygon_2>(&(m_envObsPolyList.back()));
        
        // m_scene.addItem(pAGI);
        pAGI->m_bFill = true;
        pAGI->m_edgePen = QPen(Qt::gray, 0.25, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
        pAGI->m_vertexPen = QPen(Qt::black, 0.5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
		m_PolyAGIList.push_back(pAGI);
    }

	// Then read the bounding rectangle (configuration space)
    int numberOfBoundingVertex;
    
        ifs >> numberOfBoundingVertex;
        for(int j = 0;j < numberOfBoundingVertex;j++){
        	Point_2 p;
        	double p_x, p_y;
        	ifs >> p_x >> p_y;
        	
        	p.set<0>(p_x);
        	p.set<1>(p_y);
        	bg::append(m_boundingPoly.outer(), p);
        }
        bg::correct(m_boundingPoly);

    m_envPolyVoronoiList.push_back(m_boundingPoly);
	// Add to scence

    m_pBoundingPolyAGI = new AdvancedGraphicsItem<Polygon_2>(&m_boundingPoly);
    m_pBoundingPolyAGI->m_bFill = false;
    m_pBoundingPolyAGI->m_bShowVertices = false;
    m_pBoundingPolyAGI->m_bShowEdge = true;
	m_pBoundingPolyAGI->m_edgePen = QPen(Qt::gray, 0.25, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    m_PolyAGIList.push_back(m_pBoundingPolyAGI);
    // m_scene.addItem(m_pBoundingPolyAGI);

	// Then compute the outside bounding rectangle
	double x1, y1, x2, y2;
	std::vector<Point_2> const& points = m_boundingPoly.outer();

	x1 = points[0].get<0>(); y1 = points[0].get<1>();
	x2 = points[2].get<0>(); y2 = points[2].get<1>();
	bg::append(m_boundingPoly2.outer(), Point_2(x1 - radius, y1 - radius));
	bg::append(m_boundingPoly2.outer(), Point_2(x1 - radius, y2 + radius));
	bg::append(m_boundingPoly2.outer(), Point_2(x2 + radius, y2 + radius));
	bg::append(m_boundingPoly2.outer(), Point_2(x2 + radius, y1 - radius));
//	bg::append(m_boundingPoly2.outer(), Point_2(x1 - radius, y1 - radius));
    bg::correct(m_boundingPoly2);
	// Add to scene
    m_pBoundingPolyAGI2 = new AdvancedGraphicsItem<Polygon_2>(&m_boundingPoly2);
    m_pBoundingPolyAGI2->m_bFill = false;
    m_pBoundingPolyAGI2->m_bShowVertices = false;
    m_pBoundingPolyAGI2->m_bShowEdge = true;
	m_pBoundingPolyAGI2->m_edgePen = QPen(Qt::black, 0.25, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    m_PolyAGIList.push_back(m_pBoundingPolyAGI2);
    // m_scene.addItem(m_pBoundingPolyAGI2);

	m_radius = radius;

	drawBasicEnvironment();

	// Do roadmap building setup
	m_roadmap.buildRoadmap(&m_envObsPolyList, &m_envPolyList, &m_boundingPoly, &m_envPolyVoronoiList, radius, m_scene);
	// m_roadmap.addToScene(m_scene);

	m_boundingRect = QRectF(x1 - radius*4, y1 - radius*4, x2 + radius*4, y2 + radius*4);

    // Fit to the view
  //  fitView();

	// Enable the actions for building the graph
	m_state = BUILD_STATE_MAP_OPEN;
	m_pOverlayLatticAction->setEnabled(true);
    m_pLocateBoundaryAction->setEnabled(true);
	m_pCreateAction->setEnabled(false);
	m_pSolveAction->setEnabled(false);
	m_pPlayAction->setEnabled(false);

}

void MainWindow::restore(){
    m_scene.clear();
    m_boundingPoly.clear();
    m_boundingPoly2.clear();
    m_envPolyList.clear();
    m_envObsPolyList.clear();
    m_PolyAGIList.clear(); /* The element pointers are gone when the scene object is cleared */
	if (m_fileName.length() <= 0) {
		std::cout << "filename doesnot exist" << endl;
		return;
	}
	std::cout << "fileName:" << m_fileName;
    // Reading in the environment, first the raidus of the (disc) robot
    std::ifstream ifs(m_fileName);
    double radius;
    ifs >> radius;

    // Then the number of obstacle polygons
    int numberOfPolygons;
    ifs >> numberOfPolygons;
    
    // Then read in all obstacle polygons and compute the configuration space for a single disc
    for(int i = 0; i < numberOfPolygons; i ++){
        // Load polygon
        Polygon_2 tp;
        int numberOfVertex;
        ifs >> numberOfVertex;
        //Point_2 firstOne_1;
        for(int j = 0;j < numberOfVertex;j++){
            Point_2 p;
            double p_x, p_y;
            ifs >> p_x >> p_y;
            // if(j == 0){
   //           firstOne_1.set<0>(p_x);
   //           firstOne_1.set<1>(p_y);
   //       }
            p.set<0>(p_x);
            p.set<1>(p_y);
            bg::append(tp.outer(), p);
        }
       // bg::append(tp.outer(), firstOne_1);
        bg::correct(tp);
        m_envPolyList.push_back(tp);


        // Add raw obstacle to scene and set fill to be true with fill transparency
        AdvancedGraphicsItem<Polygon_2>* pAGI = new AdvancedGraphicsItem<Polygon_2>(&(m_envPolyList.back()));
        
        // m_scene.addItem(pAGI);
        pAGI->m_bShowEdge = true;
        pAGI->m_bShowVertices = true;
        pAGI->m_bFill = false;
        pAGI->m_fillBrush = QColor(16,16,16,192);
        m_PolyAGIList.push_back(pAGI);

        // Computing the Minkowski sum
        int split_num = 8;
        Polygon_2 ep = growPolygonByRadius(tp, radius, split_num);
        bg::correct(ep);
        //split_num = 4;
        //Polygon_2 vp = growPolygonByRadius(tp, radius, split_num);
        //bg::correct(vp);

        m_envPolyVoronoiList.push_back(ep);
        m_envObsPolyList.push_back(ep);

        // Add the configuration space obstacle to the scene
        pAGI = new AdvancedGraphicsItem<Polygon_2>(&(m_envObsPolyList.back()));
        
        // m_scene.addItem(pAGI);
        pAGI->m_bFill = true;
        pAGI->m_edgePen = QPen(Qt::gray, 0.25, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
        pAGI->m_vertexPen = QPen(Qt::black, 0.5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
        m_PolyAGIList.push_back(pAGI);
    }

    // Then read the bounding rectangle (configuration space)
    int numberOfBoundingVertex;
    
        ifs >> numberOfBoundingVertex;
        for(int j = 0;j < numberOfBoundingVertex;j++){
            Point_2 p;
            double p_x, p_y;
            ifs >> p_x >> p_y;
            
            p.set<0>(p_x);
            p.set<1>(p_y);
            bg::append(m_boundingPoly.outer(), p);
        }
        bg::correct(m_boundingPoly);

    m_envPolyVoronoiList.push_back(m_boundingPoly);
    // Add to scence

    m_pBoundingPolyAGI = new AdvancedGraphicsItem<Polygon_2>(&m_boundingPoly);
    m_pBoundingPolyAGI->m_bFill = false;
    m_pBoundingPolyAGI->m_bShowVertices = false;
    m_pBoundingPolyAGI->m_bShowEdge = true;
    m_pBoundingPolyAGI->m_edgePen = QPen(Qt::gray, 0.25, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    m_PolyAGIList.push_back(m_pBoundingPolyAGI);
    // m_scene.addItem(m_pBoundingPolyAGI);

    // Then compute the outside bounding rectangle
    double x1, y1, x2, y2;
    std::vector<Point_2> const& points = m_boundingPoly.outer();

    x1 = points[0].get<0>(); y1 = points[0].get<1>();
    x2 = points[2].get<0>(); y2 = points[2].get<1>();
    bg::append(m_boundingPoly2.outer(), Point_2(x1 - radius, y1 - radius));
    bg::append(m_boundingPoly2.outer(), Point_2(x1 - radius, y2 + radius));
    bg::append(m_boundingPoly2.outer(), Point_2(x2 + radius, y2 + radius));
    bg::append(m_boundingPoly2.outer(), Point_2(x2 + radius, y1 - radius));
//  bg::append(m_boundingPoly2.outer(), Point_2(x1 - radius, y1 - radius));
    bg::correct(m_boundingPoly2);
    // Add to scene
    m_pBoundingPolyAGI2 = new AdvancedGraphicsItem<Polygon_2>(&m_boundingPoly2);
    m_pBoundingPolyAGI2->m_bFill = false;
    m_pBoundingPolyAGI2->m_bShowVertices = false;
    m_pBoundingPolyAGI2->m_bShowEdge = true;
    m_pBoundingPolyAGI2->m_edgePen = QPen(Qt::black, 0.25, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    m_PolyAGIList.push_back(m_pBoundingPolyAGI2);
    // m_scene.addItem(m_pBoundingPolyAGI2);

    m_radius = radius;

    drawBasicEnvironment();

    // Do roadmap building setup
    m_roadmap.buildRoadmap(&m_envObsPolyList, &m_envPolyList, &m_boundingPoly, &m_envPolyVoronoiList, radius, m_scene);
    // m_roadmap.addToScene(m_scene);

    m_boundingRect = QRectF(x1 - radius*4, y1 - radius*4, x2 + radius*4, y2 + radius*4);

    // Fit to the view
  //  fitView();

    // Enable the actions for building the graph
    m_state = BUILD_STATE_MAP_OPEN;
    m_pOverlayLatticAction->setEnabled(true);
    m_pLocateBoundaryAction->setEnabled(true);
    m_pCreateAction->setEnabled(false);
    m_pSolveAction->setEnabled(false);
    m_pPlayAction->setEnabled(false);

}


void MainWindow::drawBasicEnvironment(){
	m_PolyAGIList.clear();
	for(Polygon2_list::iterator pit = m_envPolyList.begin(); pit != m_envPolyList.end(); pit++){
		// Add raw obstacle to scene and set fill to be true with fill transparency
        AdvancedGraphicsItem<Polygon_2>* pAGI = new AdvancedGraphicsItem<Polygon_2>(&(*pit));
        pAGI->m_bShowEdge = true;
        pAGI->m_bShowVertices = false;
		pAGI->m_edgePen = QPen(Qt::red, 1.25, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
        pAGI->m_bFill = true;
        pAGI->m_fillBrush = QColor(16,16,16,192);
		m_PolyAGIList.push_back(pAGI);
        
    }

	for(Polygon2_list::iterator pit = m_envObsPolyList.begin(); pit != m_envObsPolyList.end(); pit++){
		// Add the configuration space obstacle to the scene
        AdvancedGraphicsItem<Polygon_2>* pAGI = new AdvancedGraphicsItem<Polygon_2>(&(*pit));
        pAGI->m_bFill = false;
		pAGI->m_bShowEdge = true;
		pAGI->m_bShowVertices = true;
        pAGI->m_edgePen = QPen(Qt::red, 0.25, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
        pAGI->m_vertexPen = QPen(Qt::black, 0.5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
		m_PolyAGIList.push_back(pAGI);

    }

	// Add to scence
    m_pBoundingPolyAGI = new AdvancedGraphicsItem<Polygon_2>(&m_boundingPoly);
    m_pBoundingPolyAGI->m_bFill = false;
    m_pBoundingPolyAGI->m_bShowVertices = false;
    m_pBoundingPolyAGI->m_bShowEdge = true;
	m_pBoundingPolyAGI->m_edgePen = QPen(Qt::gray, 0.25, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    m_PolyAGIList.push_back(m_pBoundingPolyAGI);

	// Add to scene
    m_pBoundingPolyAGI2 = new AdvancedGraphicsItem<Polygon_2>(&m_boundingPoly2);
    m_pBoundingPolyAGI2->m_bFill = false;
    m_pBoundingPolyAGI2->m_bShowVertices = false;
    m_pBoundingPolyAGI2->m_bShowEdge = true;
	m_pBoundingPolyAGI2->m_edgePen = QPen(Qt::black, 0.25, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    m_PolyAGIList.push_back(m_pBoundingPolyAGI2);

	for(std::vector<AdvancedGraphicsItem<Polygon_2> *>::iterator pagiItor = m_PolyAGIList.begin();
		pagiItor != m_PolyAGIList.end(); pagiItor ++){
        AdvancedGraphicsItem<Polygon_2>* pAGI = *pagiItor;
        m_scene.addItem(pAGI);
    }
}


void MainWindow::overlayLattice(){
	m_scene.clear();
	m_roadmap.buildHexgaonLattice();
	drawBasicEnvironment();
	m_roadmap.drawHexagonLattice(m_scene, true);
    m_roadmap.drawVoronoiDiagram(m_scene, true);
	// Set build state and enable the actions for building the graph
	m_state = BUILD_STATE_LATTICE_OVERLAYED;
	m_pOverlayLatticAction->setEnabled(false);
}

void MainWindow::locateBoundingLatticeCycle(bool doConnection){
	// Clean up
	m_scene.clear();
    m_roadmap.drawVoronoiDiagram(m_scene, true);
	// Build depending on the build state
	switch(m_state){
	case BUILD_STATE_MAP_OPEN:
		m_roadmap.buildHexgaonLattice();
		m_pOverlayLatticAction->setEnabled(false);
	case BUILD_STATE_LATTICE_OVERLAYED: 
        std::cout<<"|||||||||||||||||||||||||||||||go tp removeExcessEdges"<<std::endl;
		m_roadmap.removeExcessEdges(doConnection);
		m_pLocateBoundaryAction->setEnabled(false);
	default:;
	}
    
	// Draw environment

	drawBasicEnvironment();

	// Draw the lattice (now truncated)
	m_roadmap.drawHexagonLattice(m_scene, true);
#ifdef _DEBUG
	m_roadmap.drawBoundingCycle(m_scene);
	//m_roadmap.drawJointBoundingCycle(m_scene);
    m_roadmap.drawVertexIds(m_scene);
#endif

	// Set build state
	m_state = BUILD_STATE_TRIMMED;   

	// Enable random instance creation
	m_pCreateAction->setEnabled(true); 
    if(m_pBoundingPolyAGI2 != 0){
        m_pView->setSceneRect(m_pBoundingPolyAGI2->boundingRect());
        m_pView->fitInView(m_boundingRect, Qt::KeepAspectRatio);
    }
}

// Create and solve a random problem over current graph
void MainWindow::createRandomProblem() {
	// Stop any ongoing timer
	m_pTimer->stop();

	// Temporarily disable the action
	m_pCreateAction->setEnabled(false);
	m_pApp->processEvents();

	// Do clean up
	m_pRobotItemVec.clear();
	m_pTextItemVec.clear();
	m_svVec.clear();
	m_gvVec.clear();
	m_pathMap.clear();
	m_pPathLineItems.clear();
	
	// Get number of robots
	m_numRobots = m_pLineEdit->text().toInt();
	double spacing = m_pMinDistLineEdit->text().toDouble();

	srand (time(NULL));

	// Setup problem 
	vector<pair<double, double> > vvg;
	vector<int> svv;
	vector<int> gvv;
    vector<Point_2> svvPt;
    vector<Point_2> gvvPt;
	while(true){
		try{
			// Create random start and goal vertices
			m_svVec.clear(); m_gvVec.clear(); svv.clear(); gvv.clear();
			m_roadmap.createRandomStartGoalPairs(m_numRobots, spacing, m_svVec, m_gvVec);

			// Snap to graph
			m_sgVec.clear();
			m_roadmap.snapToGraph(m_svVec, svv, svvPt);
			m_roadmap.snapToGraph(m_gvVec, gvv, gvvPt);
			break;
		}
		catch(...){
		}
	}

    m_largestSnapLength = 0;
    double length = 0;
	for(int r = 0;  r < m_numRobots; r ++){
        double x = m_svVec[r].first;
        double y = m_svVec[r].second;
        Point_2 p0 = Point_2(x, y);
        x = m_gvVec[r].first;
        y = m_gvVec[r].second;
        Point_2 p1 = Point_2(x, y);
        length += bg::distance(p0, svvPt[r]);
        length += bg::distance(p1, gvvPt[r]);
        if(length > m_largestSnapLength){
            m_largestSnapLength = length;
        }
        length = 0;
		m_sgVec.push_back(pair<int, int>(svv[r], gvv[r]));
	}            
#ifdef _DEBUG
	for(int r = 0; r < m_numRobots; r ++){
		cout << setw(3) << svv[r];
	}
	cout << endl;
	for(int r = 0; r < m_numRobots; r ++){
		cout << setw(3) << gvv[r];
	}
	cout << endl;
#endif
	
	// Draw start and end robot locations
	m_scene.clear();
	drawBasicEnvironment();
        m_roadmap.drawHexagonLattice(m_scene, true);

#ifdef _DEBUG
	m_roadmap.drawBoundingCycle(m_scene);
	m_roadmap.drawVertexIds(m_scene);
#endif

	// Draw the robots
	QFont font;
	QPainterPath path;
	font.setPointSizeF(m_radius/1.5);
	font.setBold(true);

	for(int r = 0; r < m_numRobots; r ++){
		// Robots at current (start) locations
		QGraphicsEllipseItem *prs = m_scene.addEllipse(m_svVec[r].first - m_radius, m_svVec[r].second - m_radius, m_radius*2-0.2, m_radius*2-0.2, 
			QPen(Qt::black, 0.2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin),
			QBrush(Qt::blue));
		prs->setZValue(10);
		m_pRobotItemVec.push_back(prs);

		// Labels for robots at current (start) locations
		QGraphicsSimpleTextItem *ti = m_scene.addSimpleText(QString::number(r+1), font);
		ti->setPos(m_svVec[r].first - m_radius/2*(3.5/(m_radius+1)) + (r < 9 ? m_radius / 6 *(2.2/(m_radius-0.3)) :(r > 99 ? -m_radius / 6 *(2.2/(m_radius-0.3)) :0 )) , m_svVec[r].second - m_radius/2*(2./(m_radius-0.5)));
		ti->setPen(QPen(QColor(Qt::white), 0.15, Qt::SolidLine, Qt::RoundCap,Qt::RoundJoin));
		ti->setZValue(15);
		m_pTextItemVec.push_back(ti);

		// Robots at goal locations
		QGraphicsEllipseItem *prg = m_scene.addEllipse(m_gvVec[r].first - m_radius, m_gvVec[r].second - m_radius, m_radius*2-0.2, m_radius*2-0.2, 
			QPen(QColor(127, 127, 127, 64), 0.2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin),
			QBrush(QColor(255, 127, 127, 64)));
		prg->setZValue(5);

		// Labels for robots at goal locations
		ti = m_scene.addSimpleText(QString::number(r+1), font);
		ti->setPos(m_gvVec[r].first - m_radius/2*(3.5/(m_radius+1)) + (r < 9 ? m_radius / 6 *(2.2/(m_radius-0.3)) :(r > 99 ? -m_radius / 6 *(2.2/(m_radius-0.3)) :0 )), m_gvVec[r].second - m_radius/2*(2./(m_radius-0.5)));
		ti->setPen(QPen(QColor(Qt::red), 0.15, Qt::SolidLine, Qt::RoundCap,Qt::RoundJoin));
		ti->setZValue(6);
	}

	// Always build visibility graph and compute shortest possible makespan
	m_roadmap.buildVisibilityGraph();
	double maxLength = 0;
	for(int r = 0; r < m_numRobots; r ++){
		vector<pair<double, double> > path;
		double length = m_roadmap.computeShortestPath(m_svVec[r].first, m_svVec[r].second, m_gvVec[r].first, m_gvVec[r].second, path);
		cout<<r<<" robot:"<<length<<endl;
        cout<<"path: ";
        for(int k = 0;k < path.size();k++){ 
        cout<<"("<<path[k].first<<","<<path[k].second<<")";
        }
        cout<<endl;
        if(length > maxLength){ maxLength = length; } 
	}
	m_shotestPathLength = maxLength;
#ifdef _DEBUG
	cout << "Min possible makespan: " << m_shotestPathLength << endl;
#endif 

	// Update action enable/disable status
	m_pCreateAction->setEnabled(true);
	m_pSolveAction->setEnabled(true);
	m_pPlayAction->setEnabled(false);
}

void MainWindow::generateTestcases(){
    int i = 0;
    bool current_valid = true;
    vector<int> svv;
    vector<int> gvv;
    vector<Point_2> svvPt;
    vector<Point_2> gvvPt;
    int count = 0;
    while(i < 10){
        count = 50000;
        current_valid = true;
        restore();
        m_svVec.clear();
        m_gvVec.clear();
        m_pathMap.clear();
        m_pPathLineItems.clear();
        m_sgVec.clear();
        // Get number of robots
        m_numRobots = m_pLineEdit->text().toInt();
        double spacing = m_pMinDistLineEdit->text().toDouble();

        srand (time(NULL));

        // Setup problem 
        vector<pair<double, double> > vvg;

		svv.clear();
        gvv.clear();
        svvPt.clear();
        gvvPt.clear();
        while(true){
            try{
                // Create random start and goal vertices
				Sleep(600);
                m_svVec.clear(); m_gvVec.clear(); svv.clear(); gvv.clear();
                m_roadmap.createRandomStartGoalPairs(m_numRobots, spacing, m_svVec, m_gvVec);
			
                break;
            }
            catch(...){
            }
        }
        cout<<"m_svVec:"<<endl;
        for(int k = 0; k < m_svVec.size();k++){
            cout<<"("<<m_svVec[k].first<<","<<m_svVec[k].second<<")"<<endl;
        }
         cout<<"m_gvVec:"<<endl;
        for(int k = 0; k < m_gvVec.size();k++){
            cout<<"("<<m_gvVec[k].first<<","<<m_gvVec[k].second<<")"<<endl;
        }
        QFont font;
        QPainterPath path;
        font.setPointSizeF(m_radius/1.5);
        font.setBold(true);
        for(int r = 0; r < m_numRobots; r ++){
            // Robots at current (start) locations
            QGraphicsEllipseItem *prs = m_scene.addEllipse(m_svVec[r].first - m_radius, m_svVec[r].second - m_radius, m_radius*2-0.2, m_radius*2-0.2, 
                QPen(Qt::black, 0.2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin),
                QBrush(Qt::blue));
            prs->setZValue(10);
            m_pRobotItemVec.push_back(prs);

            // Labels for robots at current (start) locations
            QGraphicsSimpleTextItem *ti = m_scene.addSimpleText(QString::number(r+1), font);
            ti->setPos(m_svVec[r].first - m_radius/2*(3.5/(m_radius+1)) + (r < 9 ? m_radius / 6 *(2.2/(m_radius-0.3)) :(r > 99 ? -m_radius / 6 *(2.2/(m_radius-0.3)) :0 )) , m_svVec[r].second - m_radius/2*(2./(m_radius-0.5)));
            ti->setPen(QPen(QColor(Qt::white), 0.15, Qt::SolidLine, Qt::RoundCap,Qt::RoundJoin));
            ti->setZValue(15);
            m_pTextItemVec.push_back(ti);

            // Robots at goal locations
            QGraphicsEllipseItem *prg = m_scene.addEllipse(m_gvVec[r].first - m_radius, m_gvVec[r].second - m_radius, m_radius*2-0.2, m_radius*2-0.2, 
                QPen(QColor(127, 127, 127, 64), 0.2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin),
                QBrush(QColor(255, 127, 127, 64)));
            prg->setZValue(5);

            // Labels for robots at goal locations
            ti = m_scene.addSimpleText(QString::number(r+1), font);
            ti->setPos(m_gvVec[r].first - m_radius/2*(3.5/(m_radius+1)) + (r < 9 ? m_radius / 6 *(2.2/(m_radius-0.3)) :(r > 99 ? -m_radius / 6 *(2.2/(m_radius-0.3)) :0 )), m_gvVec[r].second - m_radius/2*(2./(m_radius-0.5)));
            ti->setPen(QPen(QColor(Qt::red), 0.15, Qt::SolidLine, Qt::RoundCap,Qt::RoundJoin));
            ti->setZValue(6);
        }
        locateBoundingLatticeCycle(false);
        m_roadmap.buildVisibilityGraph();
        double maxLength = 0;
		double length = 0;
        for(int r = 0; r < m_numRobots; r ++){
            vector<pair<double, double> > path;
            length = m_roadmap.computeShortestPath(m_svVec[r].first, m_svVec[r].second, m_gvVec[r].first, m_gvVec[r].second, path);
            if(length > maxLength){ maxLength = length; }
        }
        m_shotestPathLength = maxLength;
        //if(m_shotestPathLength > 1250)continue;
        m_state = BUILD_STATE_MAP_OPEN;
        
        m_roadmap.snapToGraph(m_svVec, svv, svvPt);
        m_roadmap.snapToGraph(m_gvVec, gvv, gvvPt);
        m_largestSnapLength = 0;
        double snap = 0;
        for(int r = 0;  r < m_numRobots; r ++){
            double x = m_svVec[r].first;
            double y = m_svVec[r].second;
            Point_2 p0 = Point_2(x, y);
            x = m_gvVec[r].first;
            y = m_gvVec[r].second;
            Point_2 p1 = Point_2(x, y);
            snap += bg::distance(p0, svvPt[r]);
            snap += bg::distance(p1, gvvPt[r]);
            if(snap > m_largestSnapLength){
                m_largestSnapLength = snap;
            }
            snap = 0;
            m_sgVec.push_back(pair<int, int>(svv[r], gvv[r]));
        }
        writeToTestfiles(i, "_noconnection", m_largestSnapLength, m_shotestPathLength);   
        m_state = BUILD_STATE_MAP_OPEN;
        m_sgVec.clear();
       
        restore();
        locateBoundingLatticeCycle(true);
        svv.clear();
        svvPt.clear();
        gvv.clear();
        gvvPt.clear();
        m_roadmap.snapToGraph(m_svVec, svv, svvPt);
        m_roadmap.snapToGraph(m_gvVec, gvv, gvvPt);
        m_largestSnapLength = 0;
        snap = 0;
        for(int r = 0;  r < m_numRobots; r ++){
            double x = m_svVec[r].first;
            double y = m_svVec[r].second;
            Point_2 p0 = Point_2(x, y);
            x = m_gvVec[r].first;
            y = m_gvVec[r].second;
            Point_2 p1 = Point_2(x, y);
            snap += bg::distance(p0, svvPt[r]);
            snap += bg::distance(p1, gvvPt[r]);
            if(snap > m_largestSnapLength){
                m_largestSnapLength = snap;
            }
            snap = 0;
            
            m_sgVec.push_back(pair<int, int>(svv[r], gvv[r]));
        }
        if(!writeToTestfiles(i, "_connection", m_largestSnapLength, m_shotestPathLength)){
            continue;
        }               
        i++;
    }
            // Snap to graph
        
     
    
}

bool MainWindow::writeToTestfiles(int i, string ifconnection, double snapLength, double visibilityLength){
    return m_roadmap.generateTestfile(m_sgVec, m_pathMap, m_fileFolder.toStdString(), m_envFileName.toStdString() + ifconnection, i, m_numRobots, snapLength, visibilityLength);
}

void MainWindow::singleTest(){
    double makespanLength = 0;
    double smoothLength = 0;
    double optimalRatio = 0;
    double makespan_connection_average = 0;
    double makespan_noconnection_average = 0;
    double smooth_connection_average = 0;
    double smooth_noconnection_average = 0;
    double snapLength = 0;
    double visibilityLength = 0;
    int num = 0;
    int shortcut_times = 0;
	std::string foldername = m_fileFolder.toStdString() + "\\testresult";
    std::string filename = m_fileFolder.toStdString()+"\\testresult"+"\\"+m_envFileName.toStdString()+"_";
    string pfString = filename + std::to_string(static_cast<long long>(m_numRobots))+".txt";
	cout<<"output file:"<<pfString<<endl;
    ofstream ps(pfString.c_str(), ios::out);
    for(int i = 0;i < 10;i ++){
        ps<<"solving "<<num+1<<" case"<<endl;
        ps<<"connection case:"<<endl;
        solveGeneratedProblem(i, "_connection", snapLength, visibilityLength, shortcut_times);
       // ps<<"visibility length is "<<m_shotestPathLength<<endl;
        makespanLength = (m_pathMap[0].size()-1)*m_radius/0.43;
        smoothLength = (m_pathMap[0].size()-3-shortcut_times*2)*m_radius/0.43*0.91 + shortcut_times*2*m_radius/0.43*0.866;
        makespanLength += snapLength;
        smoothLength += snapLength;
        ps<<"makespanLength: "<<makespanLength<<endl;
        ps<<"smoothLength: "<<smoothLength<<endl;
        ps<<"visibilityLength:"<<visibilityLength<<endl;
        optimalRatio = makespanLength / visibilityLength;
        if(optimalRatio < 1) ps<<"error happen~~~~~~~~~~~~"<<endl;
        ps<<"makespan optimalRatio is "<<optimalRatio<<endl;
        makespan_connection_average += optimalRatio;
        optimalRatio = smoothLength / visibilityLength;
        //if(optimalRatio < 1) ps<<"error happen~~~~~~~~~~~~"<<endl;
        ps<<"smooth makespan optimalRatio is "<<optimalRatio<<endl;
        smooth_connection_average += optimalRatio;


        solveGeneratedProblem(i, "_noconnection", snapLength, visibilityLength, shortcut_times);        
        ps<<"noconnection case:"<<endl;
        makespanLength = (m_pathMap[0].size()-1)*m_radius/0.43;
        makespanLength += snapLength;
        smoothLength = (m_pathMap[0].size()-3-shortcut_times*2)*m_radius/0.43*0.91 + shortcut_times*2*m_radius/0.43*0.866;
        ps<<"makespanLength: "<<makespanLength<<endl;
        ps<<"smoothLength: "<<smoothLength<<endl;
        ps<<"visibilityLength:  "<<visibilityLength<<endl;
        optimalRatio = makespanLength / visibilityLength;
        if(optimalRatio < 1) ps<<"error happen~~~~~~~~~~~~"<<endl;
        ps<<"makespan optimalRatio is "<<optimalRatio<<endl;
        makespan_noconnection_average += optimalRatio;
        optimalRatio = smoothLength / visibilityLength;
       // if(optimalRatio < 1) ps<<"error happen~~~~~~~~~~~~"<<endl;
        ps<<"smooth makespan optimalRatio is "<<optimalRatio<<endl;
        smooth_noconnection_average += optimalRatio;
        num ++;
        //while(getchar()== ' ');
    }
    makespan_connection_average = makespan_connection_average / num;
    makespan_noconnection_average = makespan_noconnection_average / num;
    smooth_connection_average = smooth_connection_average / num;
    smooth_noconnection_average = smooth_noconnection_average / num;   
    ps<<"makespan connection optimal ratio average:"<< makespan_connection_average << endl;
    ps<<"makespan no connection optimal ratio average:"<< makespan_noconnection_average << endl;
    ps<<"smooth connection optimal ratio average:"<< smooth_connection_average << endl;
    ps<<"smooth no connection optimal ratio average:"<< smooth_noconnection_average << endl;
}

void MainWindow::solveGeneratedProblem(int i, string ifconnection, double & snapLength, double& visibilityLength, int& shortcut_times){
    m_roadmap.solveGeneratedProblem(m_pathMap, m_fileFolder.toStdString(), m_envFileName.toStdString() + ifconnection, i, m_numRobots, snapLength, visibilityLength, shortcut_times);
}

void MainWindow::solveProblem() {
	// Setup animation. Note that the solution will be computed the first time animatePath()
	// is called
	m_pCreateAction->setEnabled(false);
	m_pSolveAction->setEnabled(false);
	m_roadmap.solveProblem(m_sgVec, m_pathMap, m_fileFolder.toStdString(), m_envFileName.toStdString());
	m_pPlayAction->setEnabled(true);
	m_pCreateAction->setEnabled(true);
}

void MainWindow::animate(){
	// Remove all paths line items from scence
	// for(vector<QGraphicsLineItem*>::iterator lii = m_pPathLineItems.begin(); 
	// 	lii != m_pPathLineItems.end(); lii ++)
	// {
	// 	m_scene.removeItem(*lii);
	// }
	// m_pPathLineItems.clear();

	// Disable play action
	m_pPlayAction->setEnabled(false);
	m_pApp->processEvents();

	// Sleep for half a second
	boost::this_thread::sleep(boost::posix_time::milliseconds(500));

	// Put all start to the initial location
	m_aniState = ANI_STATE_FROM_START;
	m_pathStep = 0;
	m_aniStatePercent = 0.0;
	for(int r = 0; r < m_numRobots; r++){
		m_currentStartMap[r] = m_svVec[r];

		m_currentGoalMap[r] = m_pathMap[r][0];
		double x = m_currentStartMap[r].first*(1-m_aniStatePercent) + m_currentGoalMap[r].first*m_aniStatePercent;
		double y = m_currentStartMap[r].second*(1-m_aniStatePercent) + m_currentGoalMap[r].second*m_aniStatePercent;
		m_pRobotItemVec[r]->setPos(x - m_svVec[r].first, y - m_svVec[r].second);
		m_pTextItemVec[r]->setPos(x - m_radius/2*(3.5/(m_radius+1)) + (r < 9 ? m_radius / 6*(2.2/(m_radius-0.3)):0), y - m_radius/2*(2./(m_radius-0.5)));
	}
	m_pApp->processEvents();

	// Sleep for half a second
	boost::this_thread::sleep(boost::posix_time::milliseconds(2000));

	// Start animation
	m_pTimer->start(2);
}

void MainWindow::animatePath(){
	// If we are at the beginning of a stage, set the temp starts and goals
   // double edgeLength = m_roadmap.getEdgeLength();
    int mode = 0;
	if(m_aniStatePercent == 0.0){
		for(int r = 0; r < m_numRobots; r ++){
			switch(m_aniState){
			// Snapping from start to the graph
			case ANI_STATE_FROM_START:
				m_currentStartMap[r] = m_svVec[r];
				m_currentGoalMap[r] = m_pathMap[r][0];
				break;

			case ANI_STATE_GRID_PATH:
                if(m_pathStep == 0){ 
                    mode = 0;  // beginning
                    m_currentPathMap[r] = m_roadmap.getVertexListLocationFromID(m_pathStep, m_pathMap[r][m_pathStep], m_pathMap[r][m_pathStep + 1], m_pathMap[r][m_pathStep], m_pathMap[r][m_pathStep+2], mode);
				}else if(m_pathStep == m_pathMap[0].size()-2){
                    mode = 1; //end
                    m_currentPathMap[r] = m_roadmap.getVertexListLocationFromID(m_pathStep, m_pathMap[r][m_pathStep], m_pathMap[r][m_pathStep + 1], m_pathMap[r][m_pathStep-1], m_pathMap[r][m_pathStep+1], mode);
                }else{
                    mode = 2;  // during the grid
                    m_currentPathMap[r] = m_roadmap.getVertexListLocationFromID(m_pathStep, m_pathMap[r][m_pathStep], m_pathMap[r][m_pathStep + 1], m_pathMap[r][m_pathStep-1], m_pathMap[r][m_pathStep+2], mode);
                   // cout<<"robot "<<r+1<<": current path:"<<endl;
                   // for(int k = 0;k < m_currentPathMap[r].size();k++){
                   //     cout<<"x:"<<m_currentPathMap[r][k].first<<" y:"<<m_currentPathMap[r][k].second<<" ";
                   //    }
                    cout<<endl;
                }
                m_currentStartMap[r] = m_pathMap[r][m_pathStep];
				m_currentGoalMap[r] = m_pathMap[r][m_pathStep + 1];
				break;
                

			case ANI_STATE_TO_GOAL:
				m_currentStartMap[r] = m_pathMap[r][m_pathStep];
				m_currentGoalMap[r] = m_gvVec[r];
				break;
			case ANI_STATE_COMPLETE:
			default:
				return;
			}
		}
        
        if(m_aniState == ANI_STATE_GRID_PATH){ 
            m_lengthAdditiveListMap.clear();
            m_lengthListMap.clear();
        for(int r = 0; r < m_numRobots;r ++){
            
            double totalLength = 0;
            std::vector<std::pair<double, double>> temp = m_currentPathMap[r];
            for(int i = 0;i < temp.size()-1; i++){

                double length = sqrt(pow(temp[i].first - temp[i+1].first, 2) + pow(temp[i].second - temp[i+1].second, 2));
                m_lengthListMap[r].push_back(length);
                totalLength += length;
            }
            for(int i = 0; i < temp.size()-1;i++){
                m_lengthListMap[r][i] = m_lengthListMap[r][i] / totalLength;
                m_lengthAdditiveListMap[r].push_back(0.0);
                for(int j = 0;j <= i; j++){
                    m_lengthAdditiveListMap[r][i] += m_lengthListMap[r][j];
                }
            }


	    }
        }//if(m_aniState == ANI_STATE_GRID_PATH){ 
        
    }

	// Animate
    
    if(m_aniState == ANI_STATE_FROM_START || m_aniState == ANI_STATE_TO_GOAL){
        m_aniStatePercent += 0.0400001;
        m_aniStatePercent += 0.0400001;
        for(int r = 0; r < m_numRobots; r++){
            double x = m_currentStartMap[r].first*(1-m_aniStatePercent) + m_currentGoalMap[r].first*m_aniStatePercent;
            double y = m_currentStartMap[r].second*(1-m_aniStatePercent) + m_currentGoalMap[r].second*m_aniStatePercent;
            m_pRobotItemVec[r]->setPos(x - m_svVec[r].first, y - m_svVec[r].second);
            m_pTextItemVec[r]->setPos(x - m_radius/2*(3.5/(m_radius+1)) + (r < 9 ? m_radius / 6*(2.2/(m_radius-0.3)):0), y - m_radius/2*(2./(m_radius-0.5)));
        }
    }else if(m_aniState == ANI_STATE_GRID_PATH){
        m_aniStatePercent += 0.0400001;
      
        int currentIndex = 0;
        for(int r = 0; r < m_numRobots;r ++){
            if(m_currentStartMap[r] == m_currentGoalMap[r]){
                double x = m_currentStartMap[r].first*(1-m_aniStatePercent) + m_currentGoalMap[r].first*m_aniStatePercent;
                double y = m_currentStartMap[r].second*(1-m_aniStatePercent) + m_currentGoalMap[r].second*m_aniStatePercent;
                m_pRobotItemVec[r]->setPos(x - m_svVec[r].first, y - m_svVec[r].second);
                m_pTextItemVec[r]->setPos(x - m_radius/2*(3.5/(m_radius+1)) + (r < 9 ? m_radius / 6*(2.2/(m_radius-0.3)):0), y - m_radius/2*(2./(m_radius-0.5)));
            }else{ 
                double x, y;
                if(m_aniStatePercent > 1){
                    int size = m_currentPathMap[r].size();
                    x = m_currentPathMap[r][size-1].first;
                    y = m_currentPathMap[r][size-1].second;
                    m_pRobotItemVec[r]->setPos(x - m_svVec[r].first, y - m_svVec[r].second);
                    m_pTextItemVec[r]->setPos(x - m_radius/2*(3.5/(m_radius+1)) + (r < 9 ? m_radius / 6*(2.2/(m_radius-0.3)):0), y - m_radius/2*(2./(m_radius-0.5)));
                }else{
                    std::vector<std::pair<double, double>> temp = m_currentPathMap[r];
                for(int i = 0; i < temp.size()-1;i++){
                    if(m_aniStatePercent < m_lengthAdditiveListMap[r][i]){
                        currentIndex = i;
                   //     std::cout<<"currentindex:"<<currentIndex<<std::endl;
                    //    std::cout<<"m_lengthAdditiveListMap"<<m_lengthAdditiveListMap[r][i]<<std::endl;
                    //    std::cout<<"statepercent:"<<m_aniStatePercent<<std::endl;
                        break;
                    }

                }   
                
                if(currentIndex > 0){
                    double currentStatePercent = m_aniStatePercent - m_lengthAdditiveListMap[r][currentIndex-1];
                    x = m_currentPathMap[r][currentIndex].first*(1-currentStatePercent/m_lengthListMap[r][currentIndex]) + m_currentPathMap[r][currentIndex+1].first*currentStatePercent/m_lengthListMap[r][currentIndex];
                    y = m_currentPathMap[r][currentIndex].second*(1-currentStatePercent/m_lengthListMap[r][currentIndex]) + m_currentPathMap[r][currentIndex+1].second*currentStatePercent/m_lengthListMap[r][currentIndex];
                }else{
                    x = m_currentPathMap[r][currentIndex].first*(1-m_aniStatePercent/m_lengthListMap[r][currentIndex]) + m_currentPathMap[r][currentIndex+1].first*m_aniStatePercent/m_lengthListMap[r][currentIndex];
                    y = m_currentPathMap[r][currentIndex].second*(1-m_aniStatePercent/m_lengthListMap[r][currentIndex]) + m_currentPathMap[r][currentIndex+1].second*m_aniStatePercent/m_lengthListMap[r][currentIndex];
                }
                
                m_pRobotItemVec[r]->setPos(x - m_svVec[r].first, y - m_svVec[r].second);
                m_pTextItemVec[r]->setPos(x - m_radius/2*(3.5/(m_radius+1)) + (r < 9 ? m_radius / 6*(2.2/(m_radius-0.3)):0), y - m_radius/2*(2./(m_radius-0.5)));
                }

                
            }
        }
    }
	
/*
//works with simple grid 
    m_aniStatePercent += 0.100001;
    if(m_aniState == ANI_STATE_FROM_START || m_aniState == ANI_STATE_TO_GOAL) m_aniStatePercent += 0.100001;
    for(int r = 0; r < m_numRobots; r++){
        double x = m_currentStartMap[r].first*(1-m_aniStatePercent) + m_currentGoalMap[r].first*m_aniStatePercent;
        double y = m_currentStartMap[r].second*(1-m_aniStatePercent) + m_currentGoalMap[r].second*m_aniStatePercent;
        m_pRobotItemVec[r]->setPos(x - m_svVec[r].first, y - m_svVec[r].
            second);
        m_pTextItemVec[r]->setPos(x - m_radius/2*(3.5/(m_radius+1)) + (r < 9 ? m_radius / 6*(2.2/(m_radius-0.3)):0), y - m_radius/2*(2./(m_radius-0.5)));
    }
*/
	// Check current stage perecentage
	if(m_aniStatePercent > 1.0){
		m_aniStatePercent = 0.0;
		switch(m_aniState){
		// Snapping from start to the graph
		case ANI_STATE_FROM_START:
			m_aniState = ANI_STATE_GRID_PATH;
			break;
		case ANI_STATE_GRID_PATH:
			m_pathStep ++;
			if(m_pathStep == m_pathMap[0].size() - 1){
				m_aniState = ANI_STATE_TO_GOAL;
			}
			break;
		case ANI_STATE_TO_GOAL:
			m_aniState = ANI_STATE_COMPLETE;
			m_pTimer->stop();
			m_pPlayAction->setEnabled(true);
			break;
		}
	}
}

// Fit the content to the current view port
void MainWindow::fitView(){
    if(m_pBoundingPolyAGI2 != 0){
        m_pView->setSceneRect(m_pBoundingPolyAGI2->boundingRect());
        m_pView->fitInView(m_boundingRect, Qt::KeepAspectRatio);
    }
	//generateTestcases();
		
	
	
	singleTest();
	
    
}
