/*
 *	Helper function implementations
 *
 *  Created on: Jan 30, 2015
 *  Author: Jingjin Yu
 */
#include "Boost_helper_functions.h"

// void populateApproximateDisc(Polygon_2 &poly, Point_2 &center, double radius, int segments){
// 	for(int i = 0; i < segments; i ++){
// 		poly.push_back (Point_2 ( center.get<0>() + radius*cos(i*PI*2/segments), center.y() + radius*sin(i*PI*2/segments)));
// 	}
// }



Polygon_2 growPolygonByRadius(Polygon_2 &poly, double radius, int split_num){
	// Get the disk for computing Minkowski sum
	Polygon_2 disc;
    const int points_per_circle = split_num;
    boost::geometry::strategy::buffer::distance_symmetric<double> distance_strategy(radius);
    boost::geometry::strategy::buffer::join_round join_strategy(points_per_circle);
    boost::geometry::strategy::buffer::end_round end_strategy(points_per_circle);
    boost::geometry::strategy::buffer::point_circle circle_strategy(points_per_circle);
    boost::geometry::strategy::buffer::side_straight side_strategy;
	// Do computation
	boost::geometry::model::multi_polygon<Polygon_2> result;
	boost::geometry::buffer(poly, result,
                distance_strategy, side_strategy,
                join_strategy, end_strategy, circle_strategy);


	return result[0];
}

double getPathLength(std::list<Point_2> path){
	double length = 0;
	std::list<Point_2>::iterator vit = path.begin();
	if(vit != path.end()){
		Point_2 p = *vit;
		vit++;
		for(; vit != path.end(); vit++){
			Point_2 p2 = *vit;
			length += sqrt((p2.get<0>() - p.get<0>())*(p2.get<0>() - p.get<0>()) + (p2.get<1>() - p.get<1>())*(p2.get<1>() - p.get<1>()));
			p = p2;
		}
	}
	return length;
}

double dot(Point_2 v1, Point_2 v2){
  double product;
  product = v1.get<0>()*v2.get<0>() + v1.get<1>()*v2.get<1>();
  return product;
}

double getDistance(Point_2& p1x, Point_2& p2x){
	
	Point_2 p1 = p1x;
	Point_2 p2 = p2x;
	return sqrt((p2.get<0>() - p1.get<0>())*(p2.get<0>() - p1.get<0>()) + (p2.get<1>() - p1.get<1>())*(p2.get<1>() - p1.get<1>()));
}

double getDistance(Point_2& p, double x, double y){
	return sqrt((p.get<0>() - x)*(p.get<0>() - x) + (p.get<1>() - y)*(p.get<1>() - y));
}

void getAllPairsShortestPath (Graph* pGraph, map<int, map<int, int> > &dist){
	int V = pGraph->getVertexSet().size();

    /* dist[][] will be the output matrix that will finally have the shortest 
      distances between every pair of vertices */
    int i, j, k;
 
    /* Initialize the solution matrix same as input graph matrix. Or 
       we can say the initial values of shortest distances are based
       on shortest paths considering no intermediate vertex. */
    for (i = 0; i < V; i++)
	{
        for (j = 0; j < V; j++){
            dist[i][j] = (pGraph->hasEdge(i, j) ? 1 : (i == j?0:100000000));
		}
	}
 
    /* Add all vertices one by one to the set of intermediate vertices.
      ---> Before start of a iteration, we have shortest distances between all
      pairs of vertices such that the shortest distances consider only the
      vertices in set {0, 1, 2, .. k-1} as intermediate vertices.
      ----> After the end of a iteration, vertex no. k is added to the set of
      intermediate vertices and the set becomes {0, 1, 2, .. k} */
    for (k = 0; k < V; k++)
    {
        // Pick all vertices as source one by one
        for (i = 0; i < V; i++)
        {
            // Pick all vertices as destination for the
            // above picked source
            for (j = 0; j < V; j++)
            {
                // If vertex k is on the shortest path from
                // i to j, then update the value of dist[i][j]
                if (dist[i][k] + dist[k][j] < dist[i][j])
                    dist[i][j] = dist[i][k] + dist[k][j];
            }
        }
    }
}

void convertFromBoosttoVisilibity(Polygon_2 &poly, Polygon &visiPoly, bool ccw){
	// Reverse orientation as needed
	// if((poly.is_counterclockwise_oriented() && ccw == false) || 
	//    (!poly.is_counterclockwise_oriented() && ccw)){
	// 	poly.reverse_orientation();
	// }
  bg::correct(poly);

	// Get the vertices and convert
	 
	std::vector<Point> vp;
	for(int i = 0; i < poly.outer().size()-1; i++){
		Point_2 p = poly.outer()[i];
		vp.push_back(Point(p.get<0>(), p.get<1>()));
	}

	visiPoly.set_vertices(vp);
	if (!ccw) {
		visiPoly.reverse();
	}
	// visiPoly.eliminate_redundant_vertices();
  //visiPoly.enforce_standard_form();
}

// bool boundaryInterset(ECPolygon_2 &poly1, ECPolygon_2 &poly2){
// 	// Check edge by edge
// 	for(int i = 0; i < poly1.size(); i++){
// 		ECSegment_2 seg1 = poly1.edge(i);
// 		for(int j = 0; j < poly2.size(); j ++){
// 			ECSegment_2 seg2 = poly2.edge(j);
// 			if(CGAL::do_intersect(seg1, seg2)){return true;}
// 		}
// 	}
// 	return false;
// }

// ECPoint_2 convertToExactPoint(Point_2 &p){
// 	static CGAL::Cartesian_converter<K,ICK> K_ICK_converter; 
// 	ICPoint_2 pp = K_ICK_converter(p);
// 	return ECPoint_2(pp.x(), pp.y());
// }


// ECPolygon_2 convertToExactPolygon(Polygon_2 &poly){
// 	ECPolygon_2 ecPoly;
// 	for(Polygon_2::Vertex_iterator vi = poly.vertices_begin(); vi != poly.vertices_end(); vi ++){
// 		static CGAL::Cartesian_converter<K,ICK> K_ICK_converter; 
// 		ICPoint_2 pp = K_ICK_converter(*vi);
// 		ecPoly.push_back(ECPoint_2(pp.x(), pp.y()));
// 	}
// 	return ecPoly;
// }

// double getTriangleArea(const Point_2& p1, Point_2 p2, Point_2 p3){
// 	return 1/2*fabs((p1.x()-p3.x())*(p2.y()-p1.y())-(p1.x()-p2.x())*(p3.y()-p1.y()));
// }

// double getShortestDistance(Polygon_2 p1, Polygon_2 p2, Point_2 & p, Segment_2& s){	
// 	double distance, minDistance;
// 	distance = 0;
// 	minDistance = 0;
// 	int k = 0;
// 	int i, j;
// 	for(i = 0;i < p1.size(); i++){              // traverse p1's vertex
// 		for(j = 0; j < p2.size(); j ++){        // traverse p2's edge
// 			if(k == 0){
// 				minDistance = getTriangleArea(p1.vertex(i), p2.edge(j).source(), p2.edge(j).target());
// 				p = p1.vertex(i);
// 				s = p2.edge(j);
// 				k++;
// 			}else{
// 				distance = getTriangleArea(p1.vertex(i), p2.edge(j).source(), p2.edge(j).target());
// 				if(distance < minDistance){
// 					minDistance = distance;
// 					p = p1.vertex(i);
// 					s = p2.edge(j);
// 				}
// 			}
		
// 		}
// 	}
// 	for(int i = 0;i < p2.size(); i++){              // traverse p1's vertex
// 		for(int j = 0; j < p1.size(); j ++){        // traverse p2's edge
// 			distance = getTriangleArea(p1.vertex(i), p2.edge(j).source(), p2.edge(j).target());
// 			if(distance < minDistance){
// 				minDistance = distance;
// 				p = p2.vertex(i);
// 				s = p1.edge(j);
// 			}
// 		}
// 	}
// 	return minDistance;
// }

