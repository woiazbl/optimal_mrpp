solving 1 case
connection case:
makespanLength: 1139.65
smoothLength: 965.511
visibilityLength:778.588
makespan optimalRatio is 1.46374
smooth makespan optimalRatio is 1.24008
noconnection case:
makespanLength: 1223.37
smoothLength: 990.419
visibilityLength:  778.588
makespan optimalRatio is 1.57127
smooth makespan optimalRatio is 1.27207
solving 2 case
connection case:
makespanLength: 1149.38
smoothLength: 975.236
visibilityLength:802.247
makespan optimalRatio is 1.43269
smooth makespan optimalRatio is 1.21563
noconnection case:
makespanLength: 1149.38
smoothLength: 914.233
visibilityLength:  802.247
makespan optimalRatio is 1.43269
smooth makespan optimalRatio is 1.13959
solving 3 case
connection case:
makespanLength: 1218.29
smoothLength: 1036.62
visibilityLength:884.124
makespan optimalRatio is 1.37797
smooth makespan optimalRatio is 1.17248
noconnection case:
makespanLength: 1295.52
smoothLength: 1066.6
visibilityLength:  884.124
makespan optimalRatio is 1.46531
smooth makespan optimalRatio is 1.2064
solving 4 case
connection case:
makespanLength: 1240.85
smoothLength: 1059.17
visibilityLength:820.353
makespan optimalRatio is 1.51258
smooth makespan optimalRatio is 1.29112
noconnection case:
makespanLength: 1324.57
smoothLength: 1066.6
visibilityLength:  820.353
makespan optimalRatio is 1.61463
smooth makespan optimalRatio is 1.30018
solving 5 case
connection case:
makespanLength: 1108.57
smoothLength: 938.2
visibilityLength:777.174
makespan optimalRatio is 1.42641
smooth makespan optimalRatio is 1.20719
noconnection case:
makespanLength: 1192.29
smoothLength: 952.326
visibilityLength:  777.174
makespan optimalRatio is 1.53414
smooth makespan optimalRatio is 1.22537
solving 6 case
connection case:
makespanLength: 1228.43
smoothLength: 1046.75
visibilityLength:871.076
makespan optimalRatio is 1.41024
smooth makespan optimalRatio is 1.20168
noconnection case:
makespanLength: 1228.43
smoothLength: 990.419
visibilityLength:  871.076
makespan optimalRatio is 1.41024
smooth makespan optimalRatio is 1.13701
solving 7 case
connection case:
makespanLength: 1110.11
smoothLength: 939.741
visibilityLength:755.34
makespan optimalRatio is 1.46969
smooth makespan optimalRatio is 1.24413
noconnection case:
makespanLength: 1151.97
smoothLength: 914.233
visibilityLength:  755.34
makespan optimalRatio is 1.52511
smooth makespan optimalRatio is 1.21036
solving 8 case
connection case:
makespanLength: 1079.73
smoothLength: 913.124
visibilityLength:692.25
makespan optimalRatio is 1.55974
smooth makespan optimalRatio is 1.31907
noconnection case:
makespanLength: 1037.87
smoothLength: 799.953
visibilityLength:  692.25
makespan optimalRatio is 1.49927
smooth makespan optimalRatio is 1.15558
solving 9 case
connection case:
makespanLength: 1234.98
smoothLength: 1053.3
visibilityLength:894.111
makespan optimalRatio is 1.38123
smooth makespan optimalRatio is 1.17804
noconnection case:
makespanLength: 1234.98
smoothLength: 990.419
visibilityLength:  894.111
makespan optimalRatio is 1.38123
smooth makespan optimalRatio is 1.10771
solving 10 case
connection case:
makespanLength: 1071.93
smoothLength: 905.324
visibilityLength:731.623
makespan optimalRatio is 1.46514
smooth makespan optimalRatio is 1.23742
noconnection case:
makespanLength: 1113.79
smoothLength: 876.14
visibilityLength:  731.623
makespan optimalRatio is 1.52235
smooth makespan optimalRatio is 1.19753
makespan connection optimal ratio average:1.44994
makespan no connection optimal ratio average:1.49562
smooth connection optimal ratio average:1.23068
smooth no connection optimal ratio average:1.19518
