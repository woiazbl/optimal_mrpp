solving 1 case
connection case:
makespanLength: 835.958
smoothLength: 688.191
visibilityLength:510.421
makespan optimalRatio is 1.63778
smooth makespan optimalRatio is 1.34828
noconnection case:
makespanLength: 877.819
smoothLength: 685.674
visibilityLength:  510.421
makespan optimalRatio is 1.71979
smooth makespan optimalRatio is 1.34335
solving 2 case
connection case:
makespanLength: 1000.36
smoothLength: 837.524
visibilityLength:687.46
makespan optimalRatio is 1.45516
smooth makespan optimalRatio is 1.21829
noconnection case:
makespanLength: 1000.36
smoothLength: 799.953
visibilityLength:  687.46
makespan optimalRatio is 1.45516
smooth makespan optimalRatio is 1.16364
solving 3 case
connection case:
makespanLength: 760.076
smoothLength: 619.844
visibilityLength:548.13
makespan optimalRatio is 1.38667
smooth makespan optimalRatio is 1.13083
noconnection case:
makespanLength: 801.937
smoothLength: 609.488
visibilityLength:  548.13
makespan optimalRatio is 1.46304
smooth makespan optimalRatio is 1.11194
solving 4 case
connection case:
makespanLength: 1052.05
smoothLength: 885.442
visibilityLength:687.033
makespan optimalRatio is 1.53129
smooth makespan optimalRatio is 1.28879
noconnection case:
makespanLength: 1002.75
smoothLength: 799.953
visibilityLength:  687.033
makespan optimalRatio is 1.45954
smooth makespan optimalRatio is 1.16436
solving 5 case
connection case:
makespanLength: 1180.52
smoothLength: 1002.61
visibilityLength:769.351
makespan optimalRatio is 1.53444
smooth makespan optimalRatio is 1.30319
noconnection case:
makespanLength: 1180.52
smoothLength: 952.326
visibilityLength:  769.351
makespan optimalRatio is 1.53444
smooth makespan optimalRatio is 1.23783
solving 6 case
connection case:
makespanLength: 969.833
smoothLength: 810.764
visibilityLength:650.515
makespan optimalRatio is 1.49087
smooth makespan optimalRatio is 1.24634
noconnection case:
makespanLength: 1053.55
smoothLength: 838.047
visibilityLength:  650.515
makespan optimalRatio is 1.61957
smooth makespan optimalRatio is 1.28828
solving 7 case
connection case:
makespanLength: 1062.51
smoothLength: 895.907
visibilityLength:772.405
makespan optimalRatio is 1.37559
smooth makespan optimalRatio is 1.15989
noconnection case:
makespanLength: 1062.51
smoothLength: 838.047
visibilityLength:  772.405
makespan optimalRatio is 1.37559
smooth makespan optimalRatio is 1.08498
solving 8 case
connection case:
makespanLength: 1190.51
smoothLength: 1012.6
visibilityLength:788.282
makespan optimalRatio is 1.51026
smooth makespan optimalRatio is 1.28457
noconnection case:
makespanLength: 1190.51
smoothLength: 952.326
visibilityLength:  788.282
makespan optimalRatio is 1.51026
smooth makespan optimalRatio is 1.2081
solving 9 case
connection case:
makespanLength: 1140.98
smoothLength: 966.836
visibilityLength:774.339
makespan optimalRatio is 1.47348
smooth makespan optimalRatio is 1.2486
noconnection case:
makespanLength: 1224.7
smoothLength: 990.419
visibilityLength:  774.339
makespan optimalRatio is 1.5816
smooth makespan optimalRatio is 1.27905
solving 10 case
connection case:
makespanLength: 1189.09
smoothLength: 1011.18
visibilityLength:807.011
makespan optimalRatio is 1.47345
smooth makespan optimalRatio is 1.253
noconnection case:
makespanLength: 1189.09
smoothLength: 952.326
visibilityLength:  807.011
makespan optimalRatio is 1.47345
smooth makespan optimalRatio is 1.18007
makespan connection optimal ratio average:1.4869
makespan no connection optimal ratio average:1.51924
smooth connection optimal ratio average:1.24818
smooth no connection optimal ratio average:1.20616
